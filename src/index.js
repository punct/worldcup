import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import configureStore from './configureStore'
import App from './App'

import { getGroupResults, getTodayMatches } from './api'

const REFRESH_INTERVAL = 60 * 1000
const store = configureStore()

const fetchData = () => {
  getGroupResults()
    .then(results => store.dispatch({ type: 'GROUP_STAGE', payload: results }))
  
  getTodayMatches()
    .then(results => store.dispatch({ type: 'TODAY_MATCHES', payload: results }))
}

fetchData()
setInterval(() => {
  fetchData()
}, REFRESH_INTERVAL)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
