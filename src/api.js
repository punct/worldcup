export const getGroupResults = () =>
    fetch('https://worldcup.sfg.io/teams/group_results')
    .then(results => results.json())

export const getTodayMatches = () =>
  fetch('http://worldcup.sfg.io/matches/today')
    .then(results => results.json())

export const getFlagUrl = (teamCode) =>
  `https://api.fifa.com/api/v1/picture/flags-fwc2018-4/${teamCode.toLowerCase()}`
