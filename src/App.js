import React from 'react'
import { connect } from 'react-redux'

import TodayMatches from './components/TodayMatches'
import GroupStage from './components/GroupStage'

const App = props => [
  <TodayMatches key="todayMatches" matches={props.todayMatches} />,
  <GroupStage key="groupStage" />
]

const mapState = state => ({
  groupStage: state.groupStage,
  todayMatches: state.todayMatches
})

export default connect(mapState, {})(App)
