import React from 'react'

import Match from './Match'

const TodayMatches = props => {
  return (
    <div className="container-fluid today-matches my-5">
      <div className="row justify-content-center">
      {
        props.matches
          .sort((a, b) => new Date(a.datetime) > new Date(b.datetime))
          .map(match =>
            <Match key={match.fifa_id} match={match} />
          )
      }
      </div>
    </div>
  )
}

export default TodayMatches
