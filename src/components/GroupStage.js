import React from 'react'
import { connect } from 'react-redux'

import Group from './Group'

export const GroupStage = props =>
  <div className="container-fluid justify-content-center">
    <div className="row groupstage">
      {
        props.groups.map(({group}) =>
          <Group key={group.id} name={group.letter} standings={group.teams} />
        )
      }
    </div>
  </div>

const mapState = state => ({
    groups: state.groupStage,
})
export default connect(mapState)(GroupStage)
