import React from 'react'

const Team = props =>
  <div className="team">
    {!props.displayFlagAfter && <img className="team-flag mx-2" src={props.flag} />}
    <span className="team-name">{props.name}</span>
    {props.displayFlagAfter && <img className="team-flag mx-2" src={props.flag} />}
  </div>

export default Team
