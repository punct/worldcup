import React from 'react'
import Countdown from 'react-countdown-now';

import Team from './Team'
import { getFlagUrl } from '../api'

const PrettyDate = props => {
  const date = new Intl.DateTimeFormat('en-GB', {
    timeZone: 'Europe/Bucharest',
    hourCycle: 'h24',
    year: 'numeric', 
    month: 'long', 
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
  }).format(props.date)

  return (
    date
  )
}

const Match = props => {
  const homeTeam = props.match.home_team
  const awayTeam = props.match.away_team
  const now = new Date()
  const matchTime = new Date(props.match.datetime)

  return (
    <div className="match col-12 col-md-6 col-xl-4 my-3">
      <div className="row">
        <div className="col-5 text-right">
          <Team name={homeTeam.country} flag={homeTeam.flag} />
        </div>
        {
          (now < matchTime)
          ? <div className="col-2 d-flex justify-content-around">
              <Countdown date={matchTime} daysInHours={true} />
            </div>
          : <div className="col-2 d-flex justify-content-around">
              <div className="match-home-team-score">{homeTeam.goals}</div>
              <span className="match-score-separator">-</span>
              <div className="match-away-team-score">{awayTeam.goals}</div>
            </div>
        }
        <div className="col-5">
          <Team name={awayTeam.country} flag={awayTeam.flag} displayFlagAfter={true} />
        </div>
      </div>
      {
        props.match.status === 'in progress'
        ? <div className="row justify-content-center text-primary">LIVE</div>
        : null
      }
      <div className="row flex-column align-items-center justify-content-center my-2">
        <PrettyDate date={matchTime} />
      </div>
    </div>
  )
}

export default Match
