import React from 'react'

import { getFlagUrl } from '../api';
import Team from './Team'

const Group = props =>
  <div className="panel panel-default group col-lg-6 col-xl-3">
    <div className="group-name">Group {props.name}</div>
    <table className="table table-bordered table-hover rounded group-standings">
      <tbody>
      {props.standings
        .map(({ team }, index) =>
          <tr key={index} className={(team.games_played === 3 && index < 2) ? 'table-success' : ''}>
            <td className="text-truncate">
                <Team name={team.country} flag={getFlagUrl(team.fifa_code)} />
            </td>
            <td className="text-center fw-25">{team.games_played}</td>
            <td className="text-center fw-25">{team.wins}</td>
            <td className="text-center fw-25">{team.draws}</td>
            <td className="text-center fw-25">{team.losses}</td>
            <td className="text-center fw-25">{team.goals_for}-{team.goals_against}</td>
            <td className="text-center fw-25"><b>{team.points}</b></td>
          </tr>
      )}
      </tbody>
    </table>
  </div>

export default Group
