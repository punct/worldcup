import { createStore, compose } from 'redux'
import { getFlagUrl } from './api';

const reducers = (state = { groupStage: [], todayMatches: [] }, action) => {
  switch (action.type) {
    case 'GROUP_STAGE':
      const groupStage = action.payload
      return {
        ...state,
        groupStage
      }
    case 'TODAY_MATCHES':
      const todayMatches = action.payload.map(match => ({
        ...match,
        home_team: {
          ...match.home_team,
          name: match.home_team.country,
          flag: getFlagUrl(match.home_team.code),
          score: match.home_team.goals
        },
        away_team: {
          ...match.away_team,
          name: match.away_team.country,
          flag: getFlagUrl(match.away_team.code),
          score: match.away_team.goals
        }
      }))
      return {
        ...state,
        todayMatches
      }
    default:
      return state
  }
}

const configureStore = (initialStore = {}) => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(reducers, composeEnhancers())
}

export default configureStore
